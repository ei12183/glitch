﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float timeToLive = 3f;

	public float bulletSpeed = 1f;

	public int bulletDamage = 50;

	public GameObject explosion;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeToLive -= Time.deltaTime;
		if (timeToLive <= 0) {
			Destroy (gameObject);
		}

		transform.position += transform.forward * bulletSpeed * Time.deltaTime;

	}

	void OnTriggerEnter (Collider collider)
	{
		HasHealth healthScript = collider.GetComponent<HasHealth> ();
		if (healthScript != null) {
			healthScript.takeDamage (bulletDamage);
		} else {
			Instantiate(explosion,transform.position,Quaternion.identity);
			Destroy(gameObject);
		}
	}
}

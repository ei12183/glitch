﻿using UnityEngine;
using System.Collections;

public class Shooting : MonoBehaviour {

	public GameObject bullet_prefab;
    public AudioSource bulletSound;
	public float timeCooldown = .1f;
	private bool onCooldown = false;
	private float timeOnCooldown;

	private GameVariables gameVars;

	// Use this for initialization
	void Start () {
		timeOnCooldown = timeCooldown;
		gameVars = GameObject.FindGameObjectWithTag (GameTags.gameController).GetComponent<GameVariables>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!onCooldown && Input.GetButton ("Fire1") && gameVars.alive) {
			Instantiate (bullet_prefab, transform.position + transform.forward + transform.up * 1.2f + transform.right*.5f, transform.rotation);
			onCooldown = true;

            bulletSound.PlayOneShot(bulletSound.clip);

			// Ray Casting
			/*
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 direction = hit.point - this.rigidbody.position;
                direction.y = 0.0f;
            }
            */


		} else if (onCooldown) {
			timeOnCooldown -= Time.deltaTime;
			if (timeOnCooldown <= 0)
			{
				timeOnCooldown = timeCooldown;
				onCooldown = false;
			}
		}
	}
}

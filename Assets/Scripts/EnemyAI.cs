﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

	private NavMeshAgent nav;

	private GameObject target;

	private float distance;

	// Use this for initialization
	void Start () {
	
	}

	void Awake () {
		nav = GetComponent<NavMeshAgent> ();

		distance = nav.stoppingDistance;

		target = GameObject.FindGameObjectWithTag ("Player");
	}

	// Update is called once per frame
	void Update () {

		if ( Vector3.Distance(transform.position, target.transform.position) <= distance )
		{
			Vector3 direction = (target.transform.position - transform.position).normalized;
			Quaternion lookRotation = Quaternion.LookRotation(direction);
			transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * nav.angularSpeed);
		}

		nav.destination = target.transform.position;

	}
}

﻿using UnityEngine;
using System.Collections;

public class BinaryWallAnimation : MonoBehaviour
{
    public bool binaryMode = false;
    public float yOffsetPerUpdate;
    private Vector2 offset = Vector2.zero;

    // Use this for initialization
    void Start()
    {

    }

    void FixedUpdate()
    {
        offset.x += yOffsetPerUpdate;
        offset.y += yOffsetPerUpdate;
    }

    // Update is called once per frame
    void Update()
    {
        if(binaryMode)
        {
            renderer.material.SetTextureOffset("_MainTex", offset);
            renderer.material.SetTextureOffset("_BumpMap", offset);
        }
    }
}

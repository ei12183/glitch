﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public GameObject target;
	private Vector3 distance;
	private Quaternion Qrotation;

	// Use this for initialization
	void Start () {
		distance = new Vector3 (-5, 7, -5);
		Qrotation = new Quaternion (.3f, .3f, -.1f, .9f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = target.transform.position + distance;
		transform.rotation = Qrotation;

	}
}

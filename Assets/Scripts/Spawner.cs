﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	Light haloLight;
	
	public float timeCooldown = 5f;
	private bool onCooldown = false;
	private float timeOnCooldown;

	public GameObject enemy_prefab;


	// Use this for initialization
	void Start () {
		haloLight = GetComponentInChildren<Light> ();
		timeOnCooldown = timeCooldown;
	}
	
	// Update is called once per frame
	void Update () {
		if (!onCooldown) {
			Instantiate (enemy_prefab, transform.position, transform.rotation);
			haloLight.intensity = 8f;
			onCooldown = true;
		} else if (onCooldown) {
			haloLight.intensity -= .1f;
			timeOnCooldown -= Time.deltaTime;
			if (timeOnCooldown <= 0)
			{
				timeOnCooldown = timeCooldown;
				onCooldown = false;
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameVariables : MonoBehaviour
{
    public GameObject walls;
    public bool glitchMode = false;

    private BinaryWallAnimation wallAnimation;
    private int score;
	private Rect scoreRect;

	public bool alive = true;

	public Text scoreLabel;

    // Use this for initialization
    void Start()
    {
        wallAnimation = walls.GetComponent<BinaryWallAnimation>();

        score = 0;
		scoreRect = new Rect (10, 10, 200, 50);
    }   

	public void updateScore (int amt)
	{
        score += amt;
	}

	/*
	void OnGUI ()
	{
		GUI.Label (scoreRect, "Score: " + score);
	}

*/
    // Update is called once per frame
    void Update()
    {
        scoreLabel.text = score.ToString();
    }

    void FixedUpdate()
    {
        this.walls.collider.enabled = !glitchMode;
        wallAnimation.binaryMode = glitchMode;
    }

    public void saveScore()
    {
        int[] scores = new int[10];
        bool changed = false;
        //myScore is the variable that holds the score.

        //Get all the scores.
        for (int i = 1; i <= 10; i++)
        {
            scores[i - 1] = PlayerPrefs.GetInt("Score" + i);
        }

        //Check if we beat any of the Top10 scores.
        for (int i = 0; i < 10; i++)
        {
            if (score > scores[i])
            {
                for (int j = 9; j > i; j--)
                {
                    scores[j] = scores[j - 1];
                }
                scores[i] = score;
                changed = true;
                break; //Or i=10.
            }
        }

        //If we changed the scores, update them.
        if (changed)
        {
            for (int i = 1; i <= 10; i++)
            {
                PlayerPrefs.SetInt("Score" + i, scores[i - 1]);
            }
            PlayerPrefs.SetInt("Highscore", 1);
        }
    }
}

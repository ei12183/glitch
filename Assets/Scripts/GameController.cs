﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameVariables gameVariables;
    public GameObject audioGameObject;
    public float fade = 0.02f;
    public float maxVolume = 1.0f;

    private AudioSource defaultSound;
    private AudioSource glitchSound;

    public float maxEnergyLevel = 100.0f;
	public float energyLevel;
	public Slider energySlider;
    public float energyGeneratedByKill;
    public float energyGeneratedPerSecond;

	public Texture glitchTexture;
	public Texture defaultTexture;
	public Light globalLight;

	private Color blueColor;

	private GameObject walls;

    // Use this for initialization
    void Start()
    {
		energyLevel = 100;
        AudioSource[] sounds = audioGameObject.GetComponents<AudioSource>();
        defaultSound = sounds[0];
        glitchSound = sounds[1];
        glitchSound.volume = 0.0f;
        defaultSound.volume = maxVolume;

		walls = GameObject.FindGameObjectWithTag ("Wall");

		blueColor = new Color(0,140,255,255);

        this.InvokeRepeating("GenerateEnergy", 0.0f, 1.0f);
    }

    void GenerateEnergy()
    {
        energyLevel += energyGeneratedPerSecond;
        if (energyLevel > maxEnergyLevel)
            energyLevel = maxEnergyLevel;
    }

    // Update is called once per frame
    void Update()
    {
        if (energyLevel > 0 && Input.GetButton("Glitch"))
        {
			energyLevel -= 10*Time.deltaTime;

//			walls.renderer.material = glitchMaterial.renderer.material;

			walls.renderer.material.mainTexture = glitchTexture;

			globalLight.color = Color.blue;

            gameVariables.glitchMode = true;
            if (glitchSound.volume < maxVolume)
            {
                glitchSound.volume += fade;
                defaultSound.volume -= fade;
            }
            else
            {
                glitchSound.volume = 1.0f;
                defaultSound.volume = 0.0f;
            }
        }
        else
		{
			walls.renderer.material.mainTexture = defaultTexture;
			globalLight.color = Color.white;

            gameVariables.glitchMode = false;
            if (glitchSound.volume > 0.0f)
            {
                glitchSound.volume -= fade;
                defaultSound.volume += fade;
            }
            else
            {
                glitchSound.volume = 0.0f;
                defaultSound.volume = maxVolume;
            }
        }
		
		energySlider.value = energyLevel / 100f;
            
    }

    public void EnemyKilled()
    {
        energyLevel += energyGeneratedByKill;
        if (energyLevel > maxEnergyLevel)
            energyLevel = maxEnergyLevel;
    }
}

﻿using UnityEngine;
using System.Collections;

public class HasHealth : MonoBehaviour {

	public int totalHealth;
	private int currentHealth;

	private bool alive;

	public GameObject Explosion;
    private GameController gameController;
	private GameVariables gameVars;

	// Use this for initialization
	void Start () {
		currentHealth = totalHealth;
		gameVars = GameObject.FindGameObjectWithTag (GameTags.gameController).GetComponent<GameVariables>();
        gameController = GameObject.FindGameObjectWithTag(GameTags.gameController).GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void takeDamage( int amt ) {
		currentHealth -= amt;
		if (currentHealth <= 0) {
			Destroy(gameObject);
			Instantiate(Explosion,transform.position,Quaternion.identity);
			gameVars.updateScore(10);
            gameController.EnemyKilled();
		}
	}
}

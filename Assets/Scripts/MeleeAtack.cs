﻿using UnityEngine;
using System.Collections;

public class MeleeAtack : MonoBehaviour {

	public int attackDamage = 10;

	public GameObject FBX;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter (Collision collision)
	{
		if (collision.collider.tag == "Player") {

			HasHealthPlayer healthScript = collision.collider.GetComponent<HasHealthPlayer> ();

			healthScript.takeDamage(attackDamage);

			Instantiate(FBX,collision.contacts[0].point,Quaternion.identity);

			Destroy(gameObject);
		}
	}
}

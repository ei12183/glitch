﻿using UnityEngine;
using System.Collections;

public class CollectibleSpawner : MonoBehaviour {

	public Transform[] spawnPositions;

	public float spawnProb = 1;
	
	public GameObject healthCollectible;
	public GameObject energyCollectible;

	// Update is called once per frame
	void Update () {
		int r = Random.Range (0, 1000);
		if (r < spawnProb) {
			Transform selected = spawnPositions [Random.Range (0, spawnPositions.Length)];
			
			if (Random.Range (0, 100) < 50) {
				Instantiate (healthCollectible, selected.position + selected.up, Quaternion.identity);
			} else {
				Instantiate (energyCollectible, selected.position + selected.up, Quaternion.identity);
			}

		} else {
			//Debug.Log("Not Triggered");
		}
	}
}

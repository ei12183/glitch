using UnityEngine;
using System.Collections;

public class DoneHashIDs : MonoBehaviour
{
    public int shooting;
    public int speed;
	
	void Awake ()
	{
        shooting = Animator.StringToHash("Shooting");
        speed = Animator.StringToHash("Speed");
	}
}

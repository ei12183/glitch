﻿var mainCanvas : Canvas;
var credCanvas : Canvas;
var scoreCanvas : Canvas;
var highscoreCanvas : Canvas;

var scoreText : UnityEngine.UI.Text;

function enableCanvas( main : boolean, creds : boolean, score : boolean, highscore : boolean ){
	mainCanvas.enabled = main;
	credCanvas.enabled = creds;
	scoreCanvas.enabled = score;
	highscoreCanvas.enabled = highscore;
}

function Start(){
	showMain();
	var i;
	//Ensure we have scores.
	if( !PlayerPrefs.HasKey( "Score1" ) ){
		for( i=1; i<=10; i++ ){
			PlayerPrefs.SetInt( "Score"+i, 0 );
		}
	}
	//Fill in the high-scores table.
	scoreText.text = "";
	for( i=1; i<=10; i++ ){
		scoreText.text += PlayerPrefs.GetInt( "Score"+i ) + " Score\n";
	}
    //Check if we got to the Highscores List.
	if( PlayerPrefs.GetInt( "Highscore" ) == 1 ){
        //Enable the appropriate canvas.
	    enableCanvas( false, false, false, true );
	    PlayerPrefs.SetInt( "Highscore", 0 );
	}
}

function showCreds(){
	enableCanvas( false, true, false, false );
}

function showMain(){
	enableCanvas( true, false, false, false );
}

function quitGame(){
	Application.Quit();
}

function showScores(){
	enableCanvas( false, false, true, false );
}

function playGame(){
	Application.LoadLevel( 1 );
}
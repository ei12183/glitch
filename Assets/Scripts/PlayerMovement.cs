﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    public float runSpeed = 5.5f;
    public float speedDampTime = 0.1f;
    public float turnSmoothing = 15.0f;
    public float walkSpeed = 1.5f;

    private Animator animator;
    private DoneHashIDs hash;

	private GameVariables gameVars;

    private bool starting_move = true;

    // Use this for initialization
    void Start()
    {
        this.animator = GetComponent<Animator>();
        hash = GameObject.FindGameObjectWithTag(GameTags.gameController).GetComponent<DoneHashIDs>();

        this.animator.SetLayerWeight(1, 1f);

		gameVars = GameObject.FindGameObjectWithTag (GameTags.gameController).GetComponent<GameVariables>();

		
    }

    void FixedUpdate()
    {
        if (starting_move) {
            MovementManagement(0.0f, -0.1f, false, false);
            starting_move = false;
        }
		if (gameVars.alive) {
			float hAxis = Input.GetAxis ("Horizontal");
			float vAxis = Input.GetAxis ("Vertical");
			bool shooting = Input.GetButton ("Fire1");
			bool walking = Input.GetButton ("Walk");

			MovementManagement (hAxis, vAxis, shooting, walking);
		} else {
			animator.SetBool("Alive",false);
		}
    }

    void MovementManagement(float horizontal, float vertical, bool shooting, bool walking)
    {
        // Set the sneaking parameter to the sneak input.
        animator.SetBool(hash.shooting, shooting);

        // If there is some axis input...
        if (horizontal != 0.0f || vertical != 0.0f)
        {
            // ... set the players rotation and set the speed parameter to 5.5f.
            Rotating(horizontal, vertical);

            if (!walking)
                animator.SetFloat(hash.speed, runSpeed, speedDampTime, Time.deltaTime);
            else
                animator.SetFloat(hash.speed, walkSpeed, speedDampTime, Time.deltaTime);
        }
        else
        {
            // Otherwise set the speed parameter to 0.
            animator.SetFloat(hash.speed, 0, 0.2f, Time.deltaTime);
        }

        /*if(shooting)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                Vector3 direction = hit.point - this.rigidbody.position;
                direction.y = 0.0f;
            }
        }*/
    }


    void Rotating(float horizontal, float vertical)
    {
        // Create a new vector of the horizontal and vertical inputs.
        Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);

        // Create a rotation based on this new vector assuming that up is the global y axis.
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);

        // Create a rotation that is an increment closer to the target rotation from the player's rotation.
        Quaternion newRotation = Quaternion.Lerp(rigidbody.rotation, targetRotation, turnSmoothing * Time.deltaTime);

        // Change the players rotation to this new rotation.
        rigidbody.MoveRotation(newRotation);


    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HasHealthPlayer : MonoBehaviour
{

    public int totalHealth;
    private int currentHealth;

    public GameVariables gameVars;

    public Slider healthBar;

    private GameController gc;
    private Animator animator;

    // Use this for initialization
    void Start()
    {
        currentHealth = totalHealth;
        gc = GameObject.FindGameObjectWithTag(GameTags.gameController).GetComponent<GameController>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.value = (float)currentHealth / totalHealth;

        if(!gameVars.alive)
        {
            if(animator.GetCurrentAnimatorStateInfo(0).IsName("Dead"))
            {
                Application.LoadLevel(0);
            }
        }
    }

    public void takeDamage(int amt)
    {
        currentHealth -= amt;

        if (currentHealth <= 0 && gameVars.alive)
        {
            gameVars.alive = false;
            gameVars.saveScore();
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "HealthCollectible")
        {
            currentHealth += 50;
            if (currentHealth > totalHealth)
            {
                currentHealth = totalHealth;
            }

            Destroy(collider.gameObject);
        }
        else if (collider.tag == "EnergyCollectible")
        {
            gc.energyLevel += 50;
            if (gc.energyLevel > 100)
            {
                gc.energyLevel = 100;
            }

			Destroy(collider.gameObject);
        }
    }
}
